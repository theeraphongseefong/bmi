# bmi
bmi-api build with Golang

## Table of Contents
- [Framework](#framework)
- [API Listing](#api-listing)
- [Build With](#build-with)
- [How To Run](#how-to-run)

## Framework
- [Golang](https://go.dev)
- [Echo](https://echo.labstack.com)

## API Listing
| API | Method | Endpoint |
|-----|-----|-----|
| cal bmi | POST | /api/v1/bmi |

## Build With
* Echo - Go web framework

## How To Run

- 1. run this command in terminal
```
go run app/main.go

or

docker-compose up --build
```
- 2. go to 
[http://localhost:8080/api/v1/swagger/](http://localhost:8080/api/v1/swagger/)

## Authors
- [theeraphongseefong@gmail.com](https://gitlab.com/theeraphongseefong/)
