package main

import (
	"bmi/bmi"
	"bmi/config"
	rest "bmi/internal/rest"
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"

	echoDocsV1 "bmi/docs"
)

func main() {
	// setup app config
	cfg := config.LoadConfig()

	// start
	e := ServerStart(cfg)
	ServerShutdown(e)

}

func ServerStart(cfg *config.AppConfig) *echo.Echo {
	e := echo.New()
	e.Use(middleware.TimeoutWithConfig(middleware.TimeoutConfig{
		Skipper: middleware.DefaultSkipper,
		Timeout: 1800 * time.Second,
	}))

	// Build service Layer
	svc := bmi.NewService(cfg)
	rest.NewBmiHandler(e, svc)

	e.Validator = &rest.CustomValidator{Validator: validator.New()}

	InitSwagger(e, cfg.Port)

	go func() {
		endPoint := fmt.Sprintf(":%s", cfg.Port)
		if err := e.Start(endPoint); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	return e
}

func ServerShutdown(e *echo.Echo) {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

func InitSwagger(e *echo.Echo, port string) {
	echoDocsV1.SwaggerInfo.Title = "(BMI) bmi-backend-service"
	echoDocsV1.SwaggerInfo.Description = "This is a bmi-backend-service server."
	echoDocsV1.SwaggerInfo.Version = "1.0"
	echoDocsV1.SwaggerInfo.Schemes = []string{"http", "https"}

	echoDocsV1.SwaggerInfo.Host = fmt.Sprintf(`%s:%s`, "localhost", port)

	e.GET("/api/v1/swagger/*", echoSwagger.WrapHandler)
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		Skipper: middleware.DefaultSkipper,
		AllowOrigins: []string{
			fmt.Sprintf(`http://%s`, echoDocsV1.SwaggerInfo.Host),
		},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPost, http.MethodOptions},
	}))
	fmt.Println("swag", fmt.Sprintf("http://%s/api/v1/swagger/", echoDocsV1.SwaggerInfo.Host))
}
