package bmi

import (
	"bmi/config"
	"bmi/domain"
	"context"
	"reflect"
	"testing"
)

func Test_bmiService_GetBMI(t *testing.T) {
	type fields struct {
		cfg *config.AppConfig
	}
	type args struct {
		c   context.Context
		req *domain.BMIRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *domain.BMIResponse
		wantErr bool
	}{
		{
			name: "TestThinness",
			args: args{
				req: &domain.BMIRequest{
					Height: 150,
					Weight: 38,
				},
			},
			want: &domain.BMIResponse{
				Category: "thinness",
			},
			wantErr: false,
		},
		{
			name: "TestUnderweight",
			args: args{
				req: &domain.BMIRequest{
					Height: 150,
					Weight: 41.5,
				},
			},
			want: &domain.BMIResponse{
				Category: "underweight",
			},
			wantErr: false,
		},
		{
			name: "TestNormal",
			args: args{
				req: &domain.BMIRequest{
					Height: 150,
					Weight: 45,
				},
			},
			want: &domain.BMIResponse{
				Category: "normal",
			},
			wantErr: false,
		},
		{
			name: "TestOverweight",
			args: args{
				req: &domain.BMIRequest{
					Height: 150,
					Weight: 57,
				},
			},
			want: &domain.BMIResponse{
				Category: "overweight",
			},
			wantErr: false,
		},
		{
			name: "TestObesity",
			args: args{
				req: &domain.BMIRequest{
					Height: 150,
					Weight: 70,
				},
			},
			want: &domain.BMIResponse{
				Category: "obesity",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Service{
				// cfg: tt.fields.cfg,
			}
			got, err := r.CalBMI(tt.args.c, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("bmiService.GetBMI() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("bmiService.GetBMI() = %v, want %v", got, tt.want)
			}
		})
	}
}
