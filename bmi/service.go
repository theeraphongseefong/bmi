package bmi

import (
	"bmi/config"
	"bmi/domain"
	"context"
	"math"
)

type BMIRepository interface {
	CalBMI(context.Context, *domain.BMIRequest) (*domain.BMIResponse, error)
}

type Service struct {
	cfg *config.AppConfig
}

func NewService(cfg *config.AppConfig) *Service {
	return &Service{
		cfg: cfg,
	}
}

/*
WorkFlow:
  - 1. Cal BMI
  - 2. Return Response
*/
// CalBMI implements RdService.
func (r *Service) CalBMI(c context.Context, req *domain.BMIRequest) (*domain.BMIResponse, error) {
	//State: Cal BMI
	// https://www.who.int/data/nutrition/nlis/info/malnutrition-in-women#:~:text=BMI%20<18.5%3A%20underweight,BMI%20≥30.0%3A%20obesity.
	bmi := req.Weight / math.Pow(req.Height/100, 2)

	var category string

	if bmi < 17.00 {
		category = "thinness"
	} else if bmi < 18.5 {
		category = "underweight"
	} else if bmi < 25.00 {
		category = "normal"
	} else if bmi < 30.00 {
		category = "overweight"
	} else {
		category = "obesity"
	}

	//State: Return Response
	resp := &domain.BMIResponse{
		Category: category,
	}
	return resp, nil
}
