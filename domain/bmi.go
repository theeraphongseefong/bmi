package domain

type BMIRequest struct {
	Height float64 `json:"height" validate:"required,min=0" example:"170.00"`
	Weight float64 `json:"weight" validate:"required,min=0" example:"60.00"`
}

type BMIResponse struct {
	Category string `json:"category"`
}
