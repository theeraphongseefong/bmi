package bmi

import (
	"context"
	"net/http"

	"bmi/domain"

	"github.com/labstack/echo/v4"
)

type BMIService interface {
	CalBMI(context.Context, *domain.BMIRequest) (*domain.BMIResponse, error)
}

// ArticleHandler  represent the httphandler for article
type BMIHandler struct {
	Service BMIService
}

// NewBmiHandler will initialize the articles/ resources endpoint
func NewBmiHandler(e *echo.Echo, svc BMIService) {
	handler := &BMIHandler{
		Service: svc,
	}
	v1 := e.Group("/api/v1")
	v1.POST("/bmi", handler.CalBMI)
}

// CalBMI godoc
// @Summary CalBMI
// @Description /api/v1/bmi
// @Accept json
// @Tags BMI
// @Produce json
// @Consumes application/json
// @Param Request body domain.BMIRequest true "Request body"
// @Success 200 {object} domain.MessageReturn{}
// @Failure 400 {object} domain.MessageReturn{}
// @Failure 404 {object} domain.MessageReturn{}
// @Failure 500 {object} domain.MessageReturn{}
// @Router /api/v1/bmi [post]
// CalBMI implements BMIController.
func (b *BMIHandler) CalBMI(c echo.Context) error {
	request := domain.BMIRequest{}
	response := domain.MessageReturn{Status: domain.StatusError}

	//State: bind http body and query params
	if err := c.Bind(&request); err != nil {
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}
	//State: validate body
	if err := c.Validate(request); err != nil {
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: execute
	res, err := b.Service.CalBMI(c.Request().Context(), &request)
	if err != nil {
		response.Msg = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	//State: Return
	response.Status = domain.StatusSuccess
	response.Data = res
	return c.JSON(http.StatusOK, response)
}
